﻿using GQQ.Forms;
using System;
using System.Net;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(GQQ.Forms.PdfWebView), typeof(GQQ.Droid.Renderers.PdfWebViewRenderer))]
namespace GQQ.Droid.Renderers
{
	public class PdfWebViewRenderer : WebViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement != null)
			{
				var control = Element as PdfWebView;
				Control.Settings.AllowUniversalAccessFromFileURLs = true;
				Control.LoadUrl(string.Format("file:///android_asset/pdfjs/web/viewer.html?file={0}", string.Format("file:///android_asset/content/{0}", WebUtility.UrlEncode(control.Uri))));
			}
		}
	}
}
