﻿using System;
using Android.Views;
using GQQ.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Util;

[assembly: ExportRenderer(typeof(GQQ.Forms.TouchCanvasView), typeof(GQQ.Droid.Renderers.TouchCanvasViewRenderer))]
namespace GQQ.Droid.Renderers {
	public class TouchCanvasViewRenderer : SKCanvasViewRenderer {

		TouchCanvasView _element;

		protected override void OnElementChanged(ElementChangedEventArgs<SKCanvasView> e) {
			// clean up old native control
			if (Control != null) {
				Control.Touch -= OnTouch;
			}

			// do clean up old element
			if (Element != null) {
				var oldTouchCanvas = (TouchCanvasView)Element;
				var oldTouchController = (ITouchCanvasViewController)Element;
				_element = (TouchCanvasView)Element;

				// ...
			}

			base.OnElementChanged(e);

			// set up new native control
			if (Control != null) {
				Control.Touch += OnTouch;
			}

			// set up new element
			if (e.NewElement != null) {
				var newTouchCanvas = (TouchCanvasView)Element;
				var newTouchController = (ITouchCanvasViewController)Element;

				// ...
			}
		}

		private float mDownX;
		private float mDownY;
		private float SCROLL_THRESHOLD = 10;
		private bool isOnClick = false;
		bool _isScaling = false;
		float _previousDistance = 0;

		private void OnTouch(object sender, TouchEventArgs e) {
			var touchCount = e.Event.PointerCount;
			switch (e.Event.Action & MotionEventActions.Mask) {
				case MotionEventActions.Down:
				case MotionEventActions.Pointer1Down:
				case MotionEventActions.Pointer2Down:
					if (touchCount >= 2) {
						var distance = Distance(e.Event.GetX(0), e.Event.GetX(1), e.Event.GetY(0), e.Event.GetY(1));
						_previousDistance = distance;
						_isScaling = true;
					} else {
						mDownX = e.Event.GetX();
						mDownY = e.Event.GetY();
						isOnClick = true;
					}
					break;
				case MotionEventActions.Cancel:
				case MotionEventActions.Up:
				case MotionEventActions.Pointer1Up:
				case MotionEventActions.Pointer2Up:
					if(touchCount <= 1) {
						if (isOnClick) {
							var touchController = Element as ITouchCanvasViewController;
							if (touchController != null) {
								var scale = Control.Resources.DisplayMetrics.Density;
								touchController.OnTouch(new SKPoint(e.Event.GetX() / scale, e.Event.GetY() / scale));
							}
							e.Handled = true;
						}
						_isScaling = false;
						_previousDistance = 0;
					}
					break;
				case MotionEventActions.Move:
					if (isOnClick && (Math.Abs(mDownX - e.Event.GetX()) > SCROLL_THRESHOLD || Math.Abs(mDownY - e.Event.GetY()) > SCROLL_THRESHOLD)) {
						isOnClick = false;
					}

					if (touchCount >= 2 && _isScaling) {
						var distance = Distance(e.Event.GetX(0), e.Event.GetX(1), e.Event.GetY(0), e.Event.GetY(1));
						//var scale = (distance - _previousDistance) / DispDistance();
						

						if(distance > _previousDistance) {
							_element.ZoomTo(0.1f);
						} else if(_previousDistance > distance) {
							_element.ZoomTo(-0.1f);
						}
						_previousDistance = distance;
						//scale += 1;
						//scale = scale * scale;
						//_element.ZoomTo(scale);
						//this.ZoomTo(scale, m_Width / 2, m_Height / 2);
						//this.Cutting();
					}
					break;
				default:
					break;
			}
		}

		private float DispDistance() {
			return FloatMath.Sqrt((float)_element.ParentView.Width * (float)_element.ParentView.Width + (float)_element.ParentView.Height * (float)_element.ParentView.Height);
		}

		private float Distance(float x0, float x1, float y0, float y1) {
			var x = x0 - x1;
			var y = y0 - y1;
			return FloatMath.Sqrt(x * x + y * y);
		}
	}
}
