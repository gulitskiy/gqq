﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(GQQ.Forms.FlatButton), typeof(GQQ.Droid.Renderers.FlatButtonRenderer))]
namespace GQQ.Droid.Renderers
{
	public class FlatButtonRenderer : ButtonRenderer
	{
		protected override void OnDraw(Android.Graphics.Canvas canvas)
		{
			base.OnDraw(canvas);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged(e);
		}
	}
}