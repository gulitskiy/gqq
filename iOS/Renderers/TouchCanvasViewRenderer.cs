﻿using System;
using GQQ.Forms;
using SkiaSharp.Views.Forms;
using SkiaSharp.Views.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(GQQ.Forms.TouchCanvasView), typeof(GQQ.iOS.Renderers.TouchCanvasViewRenderer))]
namespace GQQ.iOS.Renderers
{
	public class TouchCanvasViewRenderer : SKCanvasViewRenderer
	{
		private readonly UITapGestureRecognizer tapGestureRecognizer;
        private readonly UIPinchGestureRecognizer pinchGestureRecognizer;

		public TouchCanvasViewRenderer()
		{
			tapGestureRecognizer = new UITapGestureRecognizer(OnTapped);
            pinchGestureRecognizer = new UIPinchGestureRecognizer(OnPinch);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<SkiaSharp.Views.Forms.SKCanvasView> e)
		{
			// clean up old native control
			if (Control != null)
			{
				Control.RemoveGestureRecognizer(tapGestureRecognizer);
                Control.RemoveGestureRecognizer(pinchGestureRecognizer);
			}

			// do clean up old element
			if (Element != null)
			{
				var oldTouchCanvas = (TouchCanvasView)Element;
				var oldTouchController = (ITouchCanvasViewController)Element;

				// ...
			}

			base.OnElementChanged(e);

			// set up new native control
			if (Control != null)
			{
				Control.UserInteractionEnabled = true;
				Control.AddGestureRecognizer(tapGestureRecognizer);
                Control.AddGestureRecognizer(pinchGestureRecognizer);
			}


			// set up new element
			if (e.NewElement != null)
			{
				var newTouchCanvas = (TouchCanvasView)Element;
				var newTouchController = (ITouchCanvasViewController)Element;

				// ...
			}
		}

		private void OnTapped(UITapGestureRecognizer recognizer)
		{
			var touchController = Element as ITouchCanvasViewController;
			if (touchController != null)
			{
				touchController.OnTouch(recognizer.LocationInView(Control).ToSKPoint());
			}
		}

        private void OnPinch(UIPinchGestureRecognizer recognizer) {
			var el = Element as TouchCanvasView;
			if (el != null)
			{
				if (recognizer.State == UIGestureRecognizerState.Began || recognizer.State == UIGestureRecognizerState.Changed)
				{ 

					float deltaScale = (float)recognizer.Scale;


                    el.ZoomTo((deltaScale - 1));
					recognizer.Scale = 1;
				}
			}
        }
	}
}
