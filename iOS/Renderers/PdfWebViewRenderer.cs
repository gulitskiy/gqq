﻿using System;
using System.IO;
using System.Net;
using Foundation;
using GQQ.Forms;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(GQQ.Forms.PdfWebView), typeof(GQQ.iOS.Renderers.PdfWebViewRenderer))]
namespace GQQ.iOS.Renderers
{
	public class PdfWebViewRenderer : ViewRenderer<PdfWebView, UIWebView>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<PdfWebView> e)
		{
			base.OnElementChanged(e);

			if (Control == null)
			{
				SetNativeControl(new UIWebView());
			}

			if (e.OldElement != null)
			{
				// Cleanup
			}

			if (e.NewElement != null)
			{
				var control = Element as PdfWebView;
				string fileName = Path.Combine(NSBundle.MainBundle.BundlePath, string.Format("content/{0}", WebUtility.UrlEncode(control.Uri)));
				Control.LoadRequest(new NSUrlRequest(new NSUrl(fileName, false)));
				Control.ScalesPageToFit = true;
			}
		}
	}
}
