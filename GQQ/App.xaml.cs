﻿using FFImageLoading;
using Plugin.Media;
using Xamarin.Forms;

namespace GQQ {
    public partial class App : Application {
        public App() {
            InitializeComponent();
            CrossMedia.Current.Initialize().Wait();
            var startPageVm = new GQQ.ViewModels.StartPageViewModel();
            MainPage = startPageVm.Page;
            startPageVm.Success += OnLoadingSuccess;

            startPageVm.Init();
        }

        private void OnLoadingSuccess() {
            Device.BeginInvokeOnMainThread(() => {
                if (GQQ.Services.AuthService.Current.IsAuthenticated)
                    MainPage = new GQQ.ViewModels.MainPageViewModel().Page;
                else
                    MainPage = new GQQ.ViewModels.LoginPageViewModel().Page;

                GQQ.Services.AuthService.Current.AfterSignIn += AppAfterSignIn;
                GQQ.Services.AuthService.Current.AfterSignOut += AppAfterSignOut;
            });
        }

        private void AppAfterSignOut() {
            Device.BeginInvokeOnMainThread(() => {
                MainPage = new GQQ.ViewModels.LoginPageViewModel().Page;
            });
        }

        private void AppAfterSignIn() {
            Device.BeginInvokeOnMainThread(() => {
                MainPage = new GQQ.ViewModels.MainPageViewModel().Page;
            });
        }

        protected override void OnStart() {
            // Handle when your app starts
        }

        protected override void OnSleep() {
            ImageService.Instance.SetExitTasksEarly(true);
        }

        protected override void OnResume() {
            ImageService.Instance.SetExitTasksEarly(false);
        }
    }
}
