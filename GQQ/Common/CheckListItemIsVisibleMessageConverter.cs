﻿using GQQ.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GQQ.Common {
    public class CheckListItemIsVisibleMessageConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return false;
            var item = value as CheckListItem;
            if (!string.IsNullOrEmpty(item.Message) || (item.CheckListItemMessages.Count > 0 && !string.IsNullOrEmpty(item.LastCheckListItemMessage.Text))) return true;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
