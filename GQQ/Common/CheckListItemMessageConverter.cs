﻿using GQQ.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GQQ.Common {
    public class CheckListItemMessageConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return "";
            var item = value as CheckListItem;
            if (item.CheckListItemMessages.Count > 0 && !string.IsNullOrEmpty(item.LastCheckListItemMessage.Text))
                return item.LastCheckListItemMessage.Text;
            return item.Message;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
