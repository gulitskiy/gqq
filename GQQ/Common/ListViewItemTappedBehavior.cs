﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace GQQ.Common
{
	public class ListViewItemTappedBehavior : Behavior<ListView>
	{
		public static readonly BindableProperty CommandProperty = BindableProperty.Create("Command", typeof(ICommand), typeof(ListViewItemTappedBehavior), null);

		public ICommand Command
		{
			get
			{
				return (ICommand)GetValue(CommandProperty);
			}
			set
			{
				SetValue(CommandProperty, value);
			}
		}

		protected override void OnAttachedTo(ListView bindable)
		{
			base.OnAttachedTo(bindable);
			bindable.ItemTapped += OnItemTapped;
			bindable.BindingContextChanged += OnListViewBindingContextChanged;
		}

		private void OnListViewBindingContextChanged(object sender, EventArgs e)
		{
			BindingContext = (sender as View).BindingContext;
		}

		protected override void OnDetachingFrom(ListView bindable)
		{
			base.OnDetachingFrom(bindable);
			bindable.ItemTapped -= OnItemTapped;
			bindable.BindingContextChanged -= OnListViewBindingContextChanged;
		}

		void OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			if (Command == null || e.Item == null) return;

			var view = sender as ListView;
			view.SelectedItem = null;

			if (Command.CanExecute(e.Item))
				Command.Execute(e.Item);
		}
	}
}
