﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace GQQ.Common {
	public class Grouping<T> : List<T>, INotifyPropertyChanged, IGrouping {
		bool _expanded = false;
		public bool Expanded {
			get { return _expanded; }
			set {
				_expanded = value;
				OnPropertyChanged();
				OnPropertyChanged("StateIcon");
				OnPropertyChanged("ButtonText");
			}
		}
		public string StateIcon { get { return Expanded ? "fa-chevron-up" : "fa-chevron-down"; } }
		public string ButtonText { get { return Expanded ? "Скрыть завершенные" : "Показать все"; } }
		public int Id { get; set; }
		public string Title { get; set; }

		public Grouping(int id, string title, IEnumerable<T> items) {
			Id = id;
			Title = title;
			if (items != null && items.Count() > 0)
				foreach (var item in items) Add(item);
		}

		#region INotifyPropertyChanged implementation
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion
	}

	public interface IGrouping : IList, INotifyPropertyChanged {
		string StateIcon { get; }
		int Id { get; set; }
		string Title { get; set; }
		bool Expanded { get; set; }
	}
}
