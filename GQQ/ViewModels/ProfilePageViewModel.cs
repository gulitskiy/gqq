﻿using System;
using GQQ.Services;
using GQQ.Views;

namespace GQQ.ViewModels
{
    public class ProfilePageViewModel : BaseViewModel<ProfilePage>
    {
		string _userName;
		public string UserName
		{
			get { return _userName; }
			set { _userName = value; OnPropertyChanged(); }
		}

		protected override void Initialize()
		{
			UserName = AuthService.Current.User?.UserName;
		}
    }
}
