﻿﻿using System;
using GQQ.Views;
using Xamarin.Forms;

namespace GQQ.ViewModels
{
    public class MainPageViewModel : BaseViewModel<MainPage>
    {
        protected override void Initialize()
        {
            Page.Detail = new NavigationPage((new AreasPageViewModel()).Page);
            var masterPageViewModel = new MasterPageViewModel(Page.Detail.Navigation);
            Page.Master = (masterPageViewModel.Page);
            masterPageViewModel.PropertyChanged += (sender, e) => {
                Page.IsPresented = (sender as MasterPageViewModel).IsPresented;
            };

            Page.Detail.Focused += Detail_Focused;
        }

        private void Detail_Focused(object sender, FocusEventArgs e) {
            Page.IsPresented = false;
        }
    }
}
