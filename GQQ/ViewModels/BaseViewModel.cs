﻿using System;
using GQQ.Common;
using Xamarin.Forms;

namespace GQQ.ViewModels
{
    public abstract class BaseViewModel<T> : BaseViewModel where T : Page, new()
    {
        public T Page
        {
            get;
            set;
        }

        public BaseViewModel()
        {
            Page = new T();
            Initialize();
            Page.BindingContext = this;
        }

        protected virtual void Initialize()
        {

        }
    }

    public abstract class BaseViewModel<TPage, TParam> : BaseViewModel where TPage : Page, new()
	{
		public TPage Page
		{
			get;
			set;
		}

		public BaseViewModel(TParam param)
		{
			Page = new TPage();
			Initialize(param);
			Page.BindingContext = this;
		}

		protected virtual void Initialize(TParam param)
		{

		}
	}

    public abstract class BaseViewModel<TPage, TParam1, TParam2> : BaseViewModel where TPage : Page, new() {
        public TPage Page {
            get;
            set;
        }

        public BaseViewModel(TParam1 param1, TParam2 param2) {
            Page = new TPage();
            Initialize(param1, param2);
            Page.BindingContext = this;
        }

        protected virtual void Initialize(TParam1 param1, TParam2 param2) {

        }
    }

    public abstract class BaseViewModel : NotifyPropertyChanged {
    }
}
