﻿using GQQ.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GQQ.ViewModels {
    public class ImageViewPageViewModel : BaseViewModel<ImageViewPage, string, string> {
        public string ImageSource { get; set; }
        public string Title { get; set; }

        public ImageViewPageViewModel(string imageSource, string title) : base(imageSource, title) {
        }

        protected override void Initialize(string imageSource, string title) {
            ImageSource = imageSource;
            Title = title;
        }
    }
}
