﻿using System;
using System.Threading.Tasks;
using GQQ.Models;
using GQQ.Services;
using Xamarin.Forms;

namespace GQQ.ViewModels {
    public class FloorsPageViewModel : ConstructionsListPageViewModel {
        public FloorsPageViewModel(Construction param) : base(param) {
            Page.Appearing += Page_Appearing;
        }

        private void Page_Appearing(object sender, EventArgs e) {
            IsBusy = false;
        }

        protected override void OnTap(Construction item) {
            IsBusy = true;
            Task.Factory.StartNew(() => {
                var dataService = new DataService();
                dataService.GetCheckListItems(item.SyncId.Value);

                Device.BeginInvokeOnMainThread(() => {
                    try {
                        var floor = new FloorPageViewModel(item, Page.Navigation);
                    } catch (Exception ex) {
                        Page.DisplayAlert("Ошибка", ex.Message, "OK");
                        IsBusy = false;
                    }
                });
            });
        }
    }
}
