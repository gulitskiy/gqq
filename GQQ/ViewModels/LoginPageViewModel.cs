﻿using System;
using System.Threading.Tasks;
using GQQ.Services;
using GQQ.Views;
using Xamarin.Forms;

namespace GQQ.ViewModels
{
    public class LoginPageViewModel : BaseViewModel<LoginPage>
    {
        string _userName;// = "inspector1@gmail.com";
        string _userPassword;// = "P@ssw0rd";
		bool _isBusy = false;
		public string UserName { get { return _userName; } set { _userName = value; OnPropertyChanged(); } }
		public string Password { get { return _userPassword; } set { _userPassword = value; OnPropertyChanged(); } }
		public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }

		public Command LoginCommand { get; set; }

		protected override void Initialize()
		{
			LoginCommand = new Command(LoginExecute, () =>
			{
				return !IsBusy;
			});
		}

		void LoginExecute()
		{
			IsBusy = true;
			LoginCommand.ChangeCanExecute();
			Task.Run(() =>
			{
				try
				{
					var result = AuthService.Current.SignIn(UserName, Password);
					Device.BeginInvokeOnMainThread(() =>
					{
						if (!string.IsNullOrEmpty(result.Error))
							Page.DisplayAlert("Ошибка", result.Error, "OK");
					});
				}
				catch (Exception)
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						Page.DisplayAlert("Ошибка", "Повторите попытку еще раз.", "OK");
					});
				}
				finally
				{
					IsBusy = false;
					Device.BeginInvokeOnMainThread(() =>
					{
						LoginCommand.ChangeCanExecute();
					});
				}
			});
		}
    }
}
