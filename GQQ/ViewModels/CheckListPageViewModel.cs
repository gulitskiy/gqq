﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using GQQ.Common;
using GQQ.Data;
using GQQ.Models;
using GQQ.Views;
using Xamarin.Forms;
using FFImageLoading.Forms;

namespace GQQ.ViewModels {
	public class CheckListPageViewModel : BaseViewModel<CheckListPage, Construction> {
		Construction _construction;

		public string Title { get; set; }
		public List<DemandGroup> DemandGroups { get; set; }
		public ICommand DemandMoreCommand { get; set; }
		public ICommand ShowImageCommand { get; set; }
		public ICommand ShowChatCommand { get; set; }
		public ICommand AddYesCheckListItemCommand { get; set; }
		public ICommand AddNoCheckListItemCommand { get; set; }
        public ICommand FinishCommand { get; set; }

        bool _isBusy = true;
		public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }

		public bool IsCanAddCheckListItem { get { return Services.AuthService.Current.User.RoleName == "gq_ik"; } }

		public CheckListPageViewModel(Construction param)
			: base(param) {
			Page.DemandGroupsListView.ListBuilded += () => {
				IsBusy = false;
			};
			Page.DemandGroupsListView.ItemsSource = DemandGroups;
		}

		protected override void Initialize(Construction param) {
			DemandMoreCommand = new Command<Demand>((demand) => {
				Page.DisplayAlert("", demand.Description, "Закрыть");
			});

			ShowImageCommand = new Command<string>((src) => {
				Page.Navigation.PushAsync(new ImageViewPageViewModel(src, param.Title).Page);
			});

			ShowChatCommand = new Command<CheckListItem>((item) => {
				Page.Navigation.PushAsync(new ChatPageViewModel(item).Page);
			});

            FinishCommand = new Command<CachedImageEvents.SuccessEventArgs>((x) => {
                
            });

			List<Demand> demands;

			using (var db = new Database()) {
				demands = db.GetAll<Demand>();
			}

			_construction = param;

			AddYesCheckListItemCommand = new Command<Demand>((item) => {
                IsBusy = true;
				if (item.CheckListItem != null && item.CheckListItem.Status == 0) return;
				var vm = new AddCheckListItemPageViewModel(0, item.SyncId.Value, _construction.SyncId.Value);
				vm.ItemSaved += Vm_ItemSavedY;
                vm.Save();
				//Page.Navigation.PushAsync(vm.Page);
			}, x => IsCanAddCheckListItem);

			AddNoCheckListItemCommand = new Command<Demand>((item) => {
				if (item.CheckListItem != null && item.CheckListItem.Status == 1) return;
				var vm = new AddCheckListItemPageViewModel(1, item.SyncId.Value, _construction.SyncId.Value);
				vm.ItemSaved += Vm_ItemSaved;
				Page.Navigation.PushAsync(vm.Page);
			}, x => IsCanAddCheckListItem);

			var parent = demands.First(x => x.Metadata.Contains(_construction.Metadata));
			var groups = demands.Where(x => x.ParentId == parent.SyncId).ToList();
			DemandGroups = new List<DemandGroup>();
			foreach (var group in groups) {
				DemandGroups.Add(new DemandGroup(
					group.SyncId.Value,
					group.Title,
					_construction.SyncId.Value,
					demands.Where(x => x.ParentId == group.SyncId)));
			}

			Title = _construction.GetBreadcrumbs();
		}

		private void Vm_ItemSaved(CheckListItem item) {
			IsBusy = true;
			using (var db = new Database()) {
				var demand = db.GetAll<Demand>(x => x.SyncId == item.DemandId).First();
				var groupId = db.GetAll<Demand>(x => x.SyncId == demand.ParentId).First().SyncId.Value;
				var group = DemandGroups.First(x => x.Id == groupId);
				group.First(x => x.SyncId == demand.SyncId).CheckListItem = item;
			}
			Page.DemandGroupsListView.ItemsSource = null;
			Page.DemandGroupsListView.ItemsSource = DemandGroups;
		}

		private void Vm_ItemSavedY(CheckListItem item)
		{
			IsBusy = true;
			using (var db = new Database())
			{
				var demand = db.GetAll<Demand>(x => x.SyncId == item.DemandId).First();
				var groupId = db.GetAll<Demand>(x => x.SyncId == demand.ParentId).First().SyncId.Value;
				var group = DemandGroups.First(x => x.Id == groupId);
				group.First(x => x.SyncId == demand.SyncId).CheckListItem = item;
			}
			Page.DemandGroupsListView.ItemsSource = null;
			Page.DemandGroupsListView.ItemsSource = DemandGroups;
            IsBusy = false;
		}
	}

	public class DemandGroup : Grouping<Demand>, IGrouping {
		public DemandGroup(int id, string title, int constructionId, IEnumerable<Demand> items, bool expanded = false) : base(id, title, items) {
			Expanded = expanded;
			using (var db = new Database()) {
				foreach (var item in items) {
					item.CheckListItem = db
						.GetAll<CheckListItem>(x => x.DemandId == item.SyncId.Value && x.ConstructionId == constructionId)
						.OrderByDescending(x => x.CreatedAt)
						.FirstOrDefault();
					if (item.CheckListItem != null) {
						var checkListItemId = item.CheckListItem.SyncId;
						item.CheckListItem.CheckListItemMessages = db
							.GetAll<CheckListItemMessage>(x => x.ParentId == checkListItemId);
						if (item.CheckListItem.Status == 0) {
							item.Expanded = false;
							item.Resolved = true;
						}

					}
				}
			}
		}

		protected override void OnPropertyChanged([CallerMemberName] string propertyName = "") {
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals(nameof(Expanded)))
				foreach (var item in this) {
					if (item.Resolved)
						item.Expanded = Expanded;
				}
		}
	}
}
