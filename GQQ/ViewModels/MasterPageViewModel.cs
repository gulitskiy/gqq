﻿using System;
using System.Windows.Input;
using GQQ.Models;
using GQQ.Views;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;
using GQQ.Services;

namespace GQQ.ViewModels
{
    public class MasterPageViewModel : BaseViewModel<MasterPage, INavigation>
    {
        bool _isPresented = false;
        public bool IsPresented
        {
            get { return _isPresented; }
            set { _isPresented = value;  OnPropertyChanged(); }
        }
        public string Version { get; set; }

        public MasterPageViewModel(INavigation param) : base(param)
        {
        }

        public List<AppMenuItem> MenuItems { get; set; }
		public ICommand TapItemCommand { get; set; }

		protected override void Initialize(INavigation param)
		{
            Version = "Версия: 1.7";
            TapItemCommand = new Command<AppMenuItem>((item) =>
			{
				item.OnTap?.Invoke();
                IsPresented = false;
			});

			MenuItems = new List<AppMenuItem>();
			MenuItems.Add(new AppMenuItem()
			{
                Title = "Главная",
				Icon = "fa-home",
                OnTap = () => {
                    param.PopToRootAsync();
                    IsPresented = false;
				}
			});
			MenuItems.Add(new AppMenuItem()
			{
				Title = "Статистика",
				Icon = "fa-bar-chart",
                OnTap = () => param.PushAsync(new ChartsPageViewModel().Page)
            });
			MenuItems.Add(new AppMenuItem()
			{
				Title = "Несоответствия",
				Icon = "fa-exclamation-triangle",
                OnTap = () => param.PushAsync(new AllCheckListItemsPageViewModel().Page)
			});
			MenuItems.Add(new AppMenuItem()
			{
				Title = "Тех. карта",
				Icon = "fa-book",
                OnTap = () => param.PushAsync(new TechnicalMapPageViewModel().Page)
			});
			MenuItems.Add(new AppMenuItem()
			{
				Title = "Профиль",
				Icon = "fa-user",
				OnTap = () => param.PushAsync(new ProfilePageViewModel().Page)
			});
			MenuItems.Add(new AppMenuItem()
			{
				Title = "Выход",
				Icon = "fa-sign-out",
				OnTap = () => Task.Run((Action)AuthService.Current.SignOut)
			});
		}
    }
}
