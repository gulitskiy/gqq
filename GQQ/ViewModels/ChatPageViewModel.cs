﻿using GQQ.Models;
using GQQ.Services;
using GQQ.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace GQQ.ViewModels {
    public class ChatPageViewModel : BaseViewModel<ChatPage, CheckListItem> {
        public ObservableCollection<CheckListItemMessage> Messages { get; set; }
        string _message = "";
        public string Message { get { return _message; } set { _message = value; OnPropertyChanged(); } }
        public ICommand DummyCommand { get; set; }
        public ICommand SendMessageCommand { get; set; }


        bool _isNotBusy = true;
        public bool IsNotBusy { get { return _isNotBusy; } set { _isNotBusy = value; OnPropertyChanged(); } }

        bool _isBusy = false;
        public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); IsNotBusy = !value; } }

        public ChatPageViewModel(CheckListItem param) : base(param) {
        }

        protected override void Initialize(CheckListItem param) {
            Messages = new ObservableCollection<CheckListItemMessage>(param.CheckListItemMessages);
            DummyCommand = new Command<CheckListItemMessage>((item) => {
            });

            SendMessageCommand = new Command(() => {
                IsBusy = true;
                Task.Run(() => {
                    try {
                        var service = new DataService();
                        var message = service.SendMessage(Message, param.SyncId.Value, param.ConstructionId, param.DemandId);
                        if (message.SyncId > 0) {
                            Messages.Add(message);
                            Device.BeginInvokeOnMainThread(() => {
                                Page.DisplayAlert("Готово", "Комментарий добавлен.", "OK");
                            });
                            Message = null;
                        } else
                            Device.BeginInvokeOnMainThread(() => {
                                Page.DisplayAlert("Ошибка", "Комментарий не добавлен.", "OK");
                            }); 
                    } catch (Exception) {
                        Device.BeginInvokeOnMainThread(() => {
                            Page.DisplayAlert("Ошибка", "Комментарий не добавлен.", "OK");
                        });
                    } finally {
                        IsBusy = false;
                    }
                });
            });
        }
    }
}
