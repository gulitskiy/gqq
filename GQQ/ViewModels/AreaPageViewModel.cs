﻿using System;
using GQQ.Views;
using GQQ.Models;
using GQQ.Services;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GQQ.ViewModels
{
    public class AreaPageViewModel : BaseViewModel<AreaPage>
    {
		DataService _dataService;

		public Construction _area;
		public Construction Area { get { return _area; } set { _area = value; OnPropertyChanged(); } }

		bool _isBusy = true;
		public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }
		string _problemsCount;
		public string ProblemsCount { get { return _problemsCount; } set { _problemsCount = value; OnPropertyChanged(); } }
		string _activeFixCount;
		public string ActiveFixCount { get { return _activeFixCount; } set { _activeFixCount = value; OnPropertyChanged(); } }
		string _completionPercentage;
		public string CompletionPercentage { get { return _completionPercentage; } set { _completionPercentage = value; OnPropertyChanged(); } }


		public AreaPageViewModel(Construction area)
		{
			if (string.IsNullOrEmpty(area.Picture))
				area.Picture = "area_dummy_picture.jpg";

			Area = area;
			_dataService = new DataService();

			Task.Run(() =>
			{
				try
				{
					var stat = _dataService.GetStat4Construction(area.SyncId.Value);
					ProblemsCount = stat.Value2.ToString();
					ActiveFixCount = stat.Value3.ToString();
					CompletionPercentage = stat.Value4.ToString("F2");
				}
				catch (Exception ex)
				{
                    Debug.WriteLine(ex.Message);
				}
				finally
				{
					IsBusy = false;
				}
			});

		}
    }
}
