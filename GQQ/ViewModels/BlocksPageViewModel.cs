﻿using System;
using GQQ.Models;
using GQQ.Views;

namespace GQQ.ViewModels
{
    public class BlocksPageViewModel : ConstructionsListPageViewModel
    {
        public BlocksPageViewModel(Construction param) : base(param)
        {
        }

        protected override void OnTap(Construction item)
		{
			Page.Navigation.PushAsync(new EntrancesPageViewModel(item).Page);
		}
    }
}
