﻿using GQQ.Services;
using GQQ.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GQQ.ViewModels {
    public class ChartsPageViewModel : BaseViewModel<ChartsPage> {
        GetFullStatResult _data;
        public GetFullStatResult Data { get { return _data; } set { _data = value; OnPropertyChanged(); } }
        bool _isBusy = true;
        public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }

        public ChartsPageViewModel() {
            _data = new GetFullStatResult();
        }

        protected override void Initialize() {
            Task.Run(() => {
                var dataService = new DataService();
                Data = dataService.GetFullStat();
                IsBusy = false;
            });
        }
    }
}
