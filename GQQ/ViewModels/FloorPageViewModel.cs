﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GQQ.Data;
using GQQ.Forms;
using GQQ.Models;
using GQQ.Services;
using GQQ.Views;
using SkiaSharp;
using Xamarin.Forms;
using System.Windows.Input;

namespace GQQ.ViewModels {
    public class FloorPageViewModel : BaseViewModel<FloorPage, Construction, INavigation> {
        DataService _dataService;
        SKBitmap _map;

        /// <summary>
        /// Конструктивы (окна)
        /// </summary>
        /// <value>The constructives.</value>
        public IEnumerable<Construction> Constructions { get; set; }
        public string Title { get; set; }
        bool _isBusy = true;
        public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }
        double _scale = 1;

        public ICommand ZoomCommand { get; set; }

        public FloorPageViewModel(Construction param, INavigation navigation) : base(param, navigation) {
            Page.Appearing += Page_Appearing;
        }

        private void Page_Appearing(object sender, EventArgs e) {
            IsBusy = false;
        }

        protected override void Initialize(Construction param1, INavigation navigation) {
            Page.MapSKCanvasView.PaintSurface += MapSKCanvasView_PaintSurface;
            _dataService = new DataService();
            Title = param1.GetBreadcrumbs();
            if (string.IsNullOrEmpty(param1.Picture)) {
                throw new Exception("Отсутствует план этажа.");
            }

			/*var pinchGestureRecognizer = new PinchGestureRecognizer();
            pinchGestureRecognizer.PinchUpdated += PinchGestureRecognizer_PinchUpdated;
            Page.MapSKCanvasView.GestureRecognizers.Add(pinchGestureRecognizer);*/

			Page.MapSKCanvasView.Zoom += MapSKCanvasView_Zoom;

			ZoomCommand = new Command<string>((type) => {
                var scale = type == "+" ? _scale + 0.25 : _scale - 0.25;

                if (scale > 2 || scale < 0.25) return;

                _scale = (float)scale;
                Page.MapSKCanvasView.InvalidateSurface();
            });

            Task.Factory.StartNew(() => {
                try {
                    using (var db = new Database()) {

                        Constructions = db.GetAll<Construction>(x => x.ParentId == param1.SyncId);

                        foreach (var item in Constructions) {
                            item.Parent = param1;
                            Page.MapSKCanvasView.Touched += (point) => {
                                if (Math.Pow(point.X - item.CenterX.Value * _scale, 2) + Math.Pow(point.Y - item.CenterY.Value * _scale, 2) < Math.Pow(20 * _scale, 2)) {
                                    IsBusy = true;
                                    Device.BeginInvokeOnMainThread(() => {
                                        Page.Navigation.PushAsync(new CheckListPageViewModel(item).Page);
                                    });
                                }
                            };
                        }

                        using (MemoryStream stream = _dataService.GetImage(param1.Picture)) {
                            if (stream.Length == 0) throw new Exception();
                            using (SKManagedStream skStream = new SKManagedStream(stream)) {
                                _map = SKBitmap.Decode(skStream);
                            }
                        }

                        if (_map == null) throw new Exception("План этажа не загружен.");

                        Device.BeginInvokeOnMainThread(() => {
                            navigation.PushAsync(Page);
                        });
                    }
                } catch (Exception ex) {
                    throw new Exception("Невозможно загрузить план этажа.");
                }
            });
        }

		private void MapSKCanvasView_Zoom(float obj) {

			if(obj > 0) {
				if (_scale >= 2) return;
			} else if(obj < 0) {
				if (_scale <= 0.25) return;
			}
			_scale += obj;
			Page.MapSKCanvasView.InvalidateSurface();
		}

		private void PinchGestureRecognizer_PinchUpdated(object sender, PinchGestureUpdatedEventArgs e) {
            if (e.Status == GestureStatus.Running) {
                _scale += (e.Scale - 1) * _scale;
                Page.MapSKCanvasView.InvalidateSurface();
            }
        }

        public SKColor GetColor4Point(Construction construction) {
            var constructionId = construction.SyncId.Value;
            SKColor color = SKColors.Red.WithAlpha(127);
            using (var db = new Database()) {
                var checkListItems = db
                    .GetAll<CheckListItem>(x => x.ConstructionId == constructionId)
                    .OrderByDescending(x => x.CreatedAt)
                    .GroupBy(x => x.DemandId)
                    .Select(x => x.First());

                var demandsCount = db.GetDemandsCount(construction);

                if (checkListItems.Count() > 0) {
                    if (checkListItems.Count(x => x.Status == 0) == demandsCount)
                        color = SKColors.Green.WithAlpha(127);
                } else {
                    color = SKColors.Yellow.WithAlpha(127);
                }
            }
            return color;
        }

        private void MapSKCanvasView_PaintSurface(object sender, SkiaSharp.Views.Forms.SKPaintSurfaceEventArgs e) {
            var view = sender as TouchCanvasView;

            var scale = _scale;

            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = e.Surface.Canvas;
            canvas.Clear();

			var paint = new SKPaint();
            view.WidthRequest = _map.Width * scale;
            view.HeightRequest = _map.Height * scale;

            var rect = new SKRect(0, 0, (float)view.WidthRequest, (float)view.HeightRequest);


			canvas.DrawBitmap(_map, rect, paint);

            foreach (var item in Constructions) {
                SKPaint constructionsPaint = new SKPaint {
                    Style = SKPaintStyle.Fill,
                    Color = GetColor4Point(item)
                };
                canvas.DrawCircle(item.CenterX.Value * (float)scale, item.CenterY.Value * (float)scale, 20 * (float)scale, constructionsPaint);
            }
        }
    }
}
