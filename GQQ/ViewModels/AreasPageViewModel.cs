﻿using System;
using System.Collections.Generic;
using GQQ.Data;
using GQQ.Models;
using GQQ.Views;
using Xamarin.Forms;
using GQQ.Services;
using System.Linq;

namespace GQQ.ViewModels
{
    public class AreasPageViewModel : BaseViewModel<AreasPage>
    {
		public IEnumerable<Construction> Constructions { get; set; }

		public AreasPageViewModel()
		{
			foreach (var item in Constructions)
			{
				Page.Children.Add(GetAreaPage(item));
			}
		}

		public AreaPage GetAreaPage(Construction area)
		{
			var vm = new AreaPageViewModel(area);
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += TapGestureRecognizer_Tapped;
			vm.Page.Content.GestureRecognizers.Add(tapGestureRecognizer);
			return vm.Page;
		}

		private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
		{
			var view = sender as View;
			var areaPageViewModel = view.Parent.BindingContext as AreaPageViewModel;
			var blockPageViewModel = new BlocksPageViewModel(areaPageViewModel.Area);
            Page.Navigation.PushAsync(blockPageViewModel.Page);
		}

		protected override void Initialize()
		{
            using (var db = new Database())
            {
                var userAreas = AuthService.Current.User.Areas;
                Constructions = db.GetAll<Construction>(x => x.ParentId == null).Where(x => userAreas.Contains(x.SyncId.Value));
            }
		}
    }
}
