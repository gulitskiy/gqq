﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GQQ.Data;
using GQQ.Models;
using GQQ.Services;
using GQQ.Views;
using System.Windows.Input;
using Xamarin.Forms;

namespace GQQ.ViewModels
{
    public class StartPageViewModel : BaseViewModel<StartPage>
    {
        bool _isBusy = false;
        bool _isError = false;
        bool _isCanContinue = true;
        string _message = "";

        public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }
        public bool IsError { get { return _isError; } set { _isError = value; OnPropertyChanged(); } }
        public bool IsCanContinue { get { return _isCanContinue; } set { _isCanContinue = value; OnPropertyChanged(); } }
        public string Message { get { return _message; } set { _message = value; OnPropertyChanged(); } }

        public ICommand RetryCommand { get; set; }
        public ICommand ContinueCommand { get; set; }

        protected override void Initialize() {
            RetryCommand = new Command(Init);
            ContinueCommand = new Command(() => {
                Success?.Invoke();
            });
        }

        public void Init()
        {
            IsCanContinue = false;
            IsError = false;
            IsBusy = true;
            Message = "";
            Task.Run(() =>
            {
                using (var db = new Database())
                {
                    if (db.IsAppDataObsolete()) {
                        try {
                            var dataService = new DataService();
                            GetDemandsResult demandsResult = null;
                            GetConstructionsResult constructionsResult = null;

                            Task.WaitAll(
                                Task.Factory.StartNew(() => { demandsResult = dataService.GetDemands(); }),
                                Task.Factory.StartNew(() => { constructionsResult = dataService.GetConstructions(); }));

                            db.UpdateAppData(new AppData() {
                                Demands = demandsResult.ToDemands(),
                                Constructions = constructionsResult.ToConstructions()
                            });

                            Success?.Invoke();
                        } catch (Exception ex) {
                            IsError = true;
                            var appDataUpdate = db.GetAll<AppDataUpdate>()
                                                  .OrderByDescending(x => x.UpdatedAt)
                                                  .FirstOrDefault();
                            if(appDataUpdate == null) {
                                Message = "Немозможно получить информацию об объектах. Повторите попытку.";
                            } else {
                                IsCanContinue = true;
                                Message = "Данные устарели. Подключитесь к интенету и повторите попытку обновления, либо продолжите использовать устаревшие данные. Дата последнего обновления: "  
                                    + appDataUpdate.UpdatedAt.ToString("dd.MM.yyyy");
                            }
                        } finally {
                            IsBusy = false;
                        }
                    } else Success?.Invoke();
                }
                (new DataService()).DoSync();
            });
        }
        public event Action Success;
    }

	public class AppData
	{
		public IEnumerable<Demand> Demands { get; set; }
		public IEnumerable<Construction> Constructions { get; set; }
	}
}
