﻿﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using GQQ.Models;
using GQQ.Services;
using GQQ.Views;
using Plugin.Media;
using Xamarin.Forms;

namespace GQQ.ViewModels
{
    public class AddCheckListItemPageViewModel : BaseViewModel<AddCheckListItemPage>
    {
		bool _takingPhoto = false;
		object _photo;
		byte[] _photoData;
		public object Photo { get { return _photo; } set { _photo = value; OnPropertyChanged(); } }
		string _message;
		public string Message { get { return _message; } set { _message = value; OnPropertyChanged(); } }
		bool _isBusy = false;
		public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }

		public ICommand TakePhotoCommand { get; set; }
        public ICommand PickPhotoCommand { get; set; }
		public Command SaveCommand { get; set; }
		public Command CancelCommand { get; set; }
		public event Action<CheckListItem> ItemSaved;

		DataService _dataService;

		int _status;
		int _demandId;
		int _constructionId;

		public AddCheckListItemPageViewModel(int status, int demandId, int constructionId)
		{
			if (_photo == null)
				Photo = "camera.jpg";

			_status = status;
			_demandId = demandId;
			_constructionId = constructionId;
			_dataService = new DataService();
		}

		protected override void Initialize()
		{
			if (IsBusy) return;
			TakePhotoCommand = new Command(async () =>
			{
				if (_takingPhoto) return;
				_takingPhoto = true;
				if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
				{
					await Page.DisplayAlert("No Camera", ":( No camera available.", "OK");
                    _takingPhoto = false;
					return;
				}

				var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
				{
					DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Rear,
					CompressionQuality = 50,
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
				});

				if (file != null)
				{
					var stream = file.GetStream();
					_photoData = new byte[stream.Length];
					stream.Read(_photoData, 0, (int)stream.Length);
					Photo = ImageSource.FromStream(() =>
					{
						return new MemoryStream(_photoData);
					});
				}

				_takingPhoto = false;
			});

			PickPhotoCommand = new Command(async () =>
			{
				if (_takingPhoto) return;
				_takingPhoto = true;

                var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
				{
					CompressionQuality = 50,
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
				});

				if (file != null)
				{
					var stream = file.GetStream();
					_photoData = new byte[stream.Length];
					stream.Read(_photoData, 0, (int)stream.Length);
					Photo = ImageSource.FromStream(() =>
					{
						return new MemoryStream(_photoData);
					});
				}

				_takingPhoto = false;
			});

			SaveCommand = new Command(Save, () => !IsBusy);
			CancelCommand = new Command(() =>
			{
				Page.Navigation.PopAsync();
			}, () => !IsBusy);
		}

		public void Save()
		{
			IsBusy = true;
            SaveCommand.ChangeCanExecute();
            CancelCommand.ChangeCanExecute();

			var item = new CheckListItem()
			{
				ConstructionId = _constructionId,
				CreatedAt = DateTime.Now,
				DemandId = _demandId,
				Status = _status,
				Message = Message,
                Photo = _photoData!= null && _photoData.Length > 0 ?_dataService.SaveCheckListItemPhoto(_photoData) : ""
			};

            Task.Run(() => {
				try
				{
					_dataService.PostCheckListItem(item);

					Device.BeginInvokeOnMainThread(() =>
					{
						SaveCommand.ChangeCanExecute();
						CancelCommand.ChangeCanExecute();
						try
						{
							ItemSaved?.Invoke(item);
						}
						catch (Exception)
						{

						}
						Page.DisplayAlert("Готово", "Замечание сохранено.", "OK");
						//await Page.Navigation.PopAsync();
					});
				}
				catch (Exception ex)
				{
					Device.BeginInvokeOnMainThread(() =>
					{
                        Page.DisplayAlert("Ошибка", ex.Message, "OK");
                        //ItemSaved?.Invoke(item);
					});
				}
				finally
				{
					IsBusy = false;
					SaveCommand.ChangeCanExecute();
					CancelCommand.ChangeCanExecute();
				}
			});
		}
    }
}
