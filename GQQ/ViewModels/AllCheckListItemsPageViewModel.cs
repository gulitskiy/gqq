﻿using GQQ.Data;
using GQQ.Models;
using GQQ.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace GQQ.ViewModels {
    public class AllCheckListItemsPageViewModel : BaseViewModel<AllCheckListItemsPage> {
        bool _isBusy = true;
        public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }
        public List<CheckListItem> CheckListItems { get; set; }
        public ICommand TapItemCommand { get; set; }
        public ICommand ShowImageCommand { get; set; }

        protected override void Initialize() {
            TapItemCommand = new Command<CheckListItem>((item) => {
                new FloorPageViewModel(item.Construction.Parent, Page.Navigation);
            });
            ShowImageCommand = new Command<CheckListItem>((item) => {
                Page.Navigation.PushAsync(new ImageViewPageViewModel(item.Photo, item.Construction.Title).Page);
            });
            CheckListItems = new List<CheckListItem>();
            Task.Run(() => {
                using (var db = new Database()) {

                    var constructions = db.GetAll<Construction>();
                    foreach(var construction in constructions) {
                        if (construction.ParentId.HasValue)
                            construction.Parent = constructions.First(x => x.SyncId == construction.ParentId);
                    }

                    var items = db.GetAll<CheckListItem>(x => x.Status == 1)
                        .OrderByDescending(x => x.CreatedAt)
                        .GroupBy(x => new { x.DemandId, x.ConstructionId });

                    foreach(var item in items) {
                        var checkListItem = item.First();
                        checkListItem.Construction = constructions.First(x => x.SyncId == checkListItem.ConstructionId);
                        checkListItem.Demand = db.GetAll<Demand>(x => x.SyncId == checkListItem.DemandId).First();
                        CheckListItems.Add(checkListItem);
                    }
                }
                IsBusy = false;
            });
        }
    }
}
