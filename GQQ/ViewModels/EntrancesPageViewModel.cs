﻿using System;
using GQQ.Models;

namespace GQQ.ViewModels
{
    public class EntrancesPageViewModel : ConstructionsListPageViewModel
    {
        public EntrancesPageViewModel(Construction param) : base(param)
        {
            Page.Appearing += Page_Appearing;
        }

        private void Page_Appearing(object sender, EventArgs e) {
            IsBusy = false;
        }

        protected override void OnTap(Construction item)
		{
            if (!string.IsNullOrEmpty(item.Picture)) {
                IsBusy = true;
                try {
                    var floor = new FloorPageViewModel(item, Page.Navigation);
                } catch (Exception ex) {
                    Page.DisplayAlert("Ошибка", ex.Message, "OK");
                    IsBusy = false;
                }
            } else
                Page.Navigation.PushAsync(new FloorsPageViewModel(item).Page);
        }
    }
}
