﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using GQQ.Data;
using GQQ.Models;
using GQQ.Services;
using GQQ.Views;
using Xamarin.Forms;

namespace GQQ.ViewModels {
    public class ConstructionsListPageViewModel : ConstructionsListPageViewModel<ConstructionsListPage> {
        public ConstructionsListPageViewModel(Construction param) : base(param) {
        }
    }

    public class ConstructionsListPageViewModel<T> : BaseViewModel<T, Construction> where T : ConstructionsListPage, new() {
        DataService _dataService;

        public string Title { get; set; }
        public IEnumerable<Construction> Constructions { get; set; }
        public ICommand TapItemCommand { get; set; }
        bool _isBusy = false;

        public ConstructionsListPageViewModel(Construction param)
            : base(param) {
            Title = param.GetBreadcrumbs();
        }

        public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged(); } }


        protected override void Initialize(Construction param) {
            _dataService = new DataService();

            TapItemCommand = new Command<Construction>((item) => {
                OnTap(item);
            });

            using (var db = new Database()) {
                Constructions = db.GetAll<Construction>(x => x.ParentId == param.SyncId)
                .Select(x => {
                    x.Parent = param;
                    return x;
                });
            }

            Page.Appearing += Page_Appearing;
        }

        private void Page_Appearing(object sender, EventArgs e) {
            Task.Factory.StartNew(() => {
                using (var db = new Database()) {
                    foreach (var item in Constructions) {
                        var stat = _dataService.GetStat4Construction(item.SyncId.Value, db);
                        item.ProblemsCount = stat.Value2.ToString("F0");
                        item.ActiveFixCount = stat.Value3.ToString("F0");
                        item.CompletionPercentage = stat.Value4.ToString("F2");
                        item.IsStatLoading = false;
                    }
                }
            });
        }

        protected virtual void OnTap(Construction item) {
            Page.Navigation.PushAsync(new ConstructionsListPageViewModel<ConstructionsListPage>(item).Page);
        }
    }
}
