﻿using System;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Xamarin.Forms;

namespace GQQ.Forms {
	public class TouchCanvasView : SKCanvasView, ITouchCanvasViewController {
		bool _isTouched = false;
		public event Action<SKPoint> Touched;
		public event Action<float> Zoom;

		public virtual void OnTouch(SKPoint point) {
			if (_isTouched) return;
			_isTouched = true;
			Touched?.Invoke(point);
			_isTouched = false;
		}

		public void ZoomTo(float scale) {
			Zoom?.Invoke(scale);
		}

		public TouchCanvasView() {
		}

	}

	public interface ITouchCanvasViewController : IViewController {
		void OnTouch(SKPoint point);
	}
}
