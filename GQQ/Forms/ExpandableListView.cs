﻿using System;
using System.Collections.Generic;
using GQQ.Common;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace GQQ.Forms {
    public class ExpandableListView : ScrollView {
        public event Action ListBuilded;

        #region BindableProperties
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
            propertyName: "ItemsSource",
            returnType: typeof(IEnumerable<IGrouping>),
            declaringType: typeof(ExpandableListView),
            propertyChanged: OnItemsSourceChanged,
            defaultValue: null,
            defaultBindingMode: BindingMode.OneWay);

        public IEnumerable<IGrouping> ItemsSource {
            get { return (IEnumerable<IGrouping>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        static void OnItemsSourceChanged(BindableObject bindable, object oldValue, object newValue) {
            var expandableListView = bindable as ExpandableListView;
            if (newValue == null) { expandableListView.Content = null; return; }
            var content = new StackLayout();
            content.Spacing = 0;

            Task.Run(() => {
                foreach (var group in expandableListView.ItemsSource) {
                    var groupView = new ContentView();
                    var tap = new TapGestureRecognizer();
                    tap.Command = new Command(() => {
                        group.Expanded = !group.Expanded;
                    });

                    groupView.Content = expandableListView.GroupHeaderTemplate.CreateContent() as View;
                    groupView.Content.BindingContext = group;

                    groupView.GestureRecognizers.Add(tap);
                    content.Children.Add(groupView);

                    foreach (var item in group) {
                        var itemView = new ContentView();
                        itemView.Content = expandableListView.ItemTemplate.CreateContent() as View;
                        itemView.Content.BindingContext = item;
                        itemView.SetBinding(ContentView.IsVisibleProperty, new Binding(
                            path: "Expanded",
                            source: item));
                        content.Children.Add(itemView);
                        var line = new BoxView();
                        line.HeightRequest = 1;
                        line.BackgroundColor = Color.Gray;
                        line.SetBinding(BoxView.IsVisibleProperty, new Binding(
                            path: "Expanded",
                            source: item));
                        if (group.IndexOf(item) < group.Count - 1)
                            content.Children.Add(line);
                    }
                }

                Device.BeginInvokeOnMainThread(() => {
                    expandableListView.Content = content;
                    expandableListView.Orientation = ScrollOrientation.Vertical;
                    expandableListView.ListBuilded?.Invoke();
                });
            });
        }

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create(
            propertyName: "ItemTemplate",
            returnType: typeof(DataTemplate),
            declaringType: typeof(ExpandableListView),
            propertyChanged: OnItemTemplateChanged,
            defaultValue: default(DataTemplate));

        public DataTemplate ItemTemplate {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        static void OnItemTemplateChanged(BindableObject bindable, object oldValue, object newValue) {
            // Property changed implementation goes here
        }

        public static readonly BindableProperty GroupHeaderTemplateProperty = BindableProperty.Create(
            propertyName: "GroupHeaderTemplate",
            returnType: typeof(DataTemplate),
            declaringType: typeof(ExpandableListView),
            propertyChanged: OnGroupHeaderTemplateChanged,
            defaultValue: default(DataTemplate));

        public DataTemplate GroupHeaderTemplate {
            get { return (DataTemplate)GetValue(GroupHeaderTemplateProperty); }
            set { SetValue(GroupHeaderTemplateProperty, value); }
        }

        static void OnGroupHeaderTemplateChanged(BindableObject bindable, object oldValue, object newValue) {
            // Property changed implementation goes here
        }
        #endregion
    }
}
