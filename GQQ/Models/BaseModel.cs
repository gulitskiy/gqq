﻿using GQQ.Common;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GQQ.Models {
    public abstract class BaseModel : NotifyPropertyChanged {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int? SyncId { get; set; }

        public BaseModel() {
            SyncId = null;
        }
    }
}
