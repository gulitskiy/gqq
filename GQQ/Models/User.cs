﻿using System;
using System.Collections.Generic;

namespace GQQ.Models
{
	public class User
	{
		public string Token { get; set; }
		public string UserName { get; set; }
        public string RoleName { get; set; }
        public List<int> Areas { get; set; }

        public User() {
            Areas = new List<int>();
        }
    }
}
