﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GQQ.Models {
    public class Stat4Construction : BaseModel {
        public int ConstructionId { get; set; }
        public double Value1 { get; set; }
        public double Value2 { get; set; }
        public double Value3 { get; set; }
        public double Value4 { get; set; }
        public double Value5 { get; set; }
        public double Value6 { get; set; }
        public DateTime CreatedAt { get; set; }

        public Stat4Construction() {
            CreatedAt = DateTime.Now;
        }
    }
}
