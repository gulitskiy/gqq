﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GQQ.Models {
    /// <summary>
    /// Требование оно же описание замечания
    /// </summary>
    public class Demand : BaseModel {
        public int? ParentId { get; set; }
        public string Metadata { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
		public bool Resolved { get; set; } = false;
		bool _expanded = true;
		public bool Expanded {
			get { return _expanded; }
			set { _expanded = value; OnPropertyChanged(); }
		}

		[SQLite.Ignore]
        public CheckListItem CheckListItem { get; set; }
    }
}
