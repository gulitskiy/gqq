﻿using System;
namespace GQQ.Models
{
    public class AppMenuItem
    {
        public string Title
        {
            get;
            set;
        }
        public string Icon
        {
            get;
            set;
        }
        public Action OnTap
        {
            get;
            set;
        }
    }
}
