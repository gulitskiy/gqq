﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GQQ.Models {
    public class CheckListItem : BaseModel {
        /// <summary>
        /// descriptionId
        /// </summary>
        public int DemandId { get; set; }
        /// <summary>
        /// areaId
        /// </summary>
        public int ConstructionId { get; set; }
        public string Photo { get; set; }
        /// <summary>
        /// yes ? 0 : 1
        /// </summary>
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Message { get; set; }

        [SQLite.Ignore]
        public Construction Construction { get; set; }
        [SQLite.Ignore]
        public Demand Demand { get; set; }
        [SQLite.Ignore]
        public string Path { get { return Construction?.GetBreadcrumbs(); } }
        [SQLite.Ignore]
        public List<CheckListItemMessage> CheckListItemMessages { get; set; }
        [SQLite.Ignore]
        public CheckListItemMessage LastCheckListItemMessage {
            get {
                return CheckListItemMessages.OrderByDescending(x => x.CreatedAt).FirstOrDefault();
            }
        }

        public CheckListItem() {
            CheckListItemMessages = new List<CheckListItemMessage>();
        }
    }
}
