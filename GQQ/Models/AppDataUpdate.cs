﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GQQ.Models {
    public class AppDataUpdate : BaseModel {
        public DateTime UpdatedAt { get; set; }

        public AppDataUpdate() {
            UpdatedAt = DateTime.Now;
        }
    }
}
