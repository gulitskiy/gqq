﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GQQ.Models {
    public class Construction : BaseModel {
        public int? ParentId { get; set; }
        public string Title { get; set; }
        public string Picture { get; set; }
        public float? CenterX { get; set; }
        public float? CenterY { get; set; }
        public string Metadata { get; set; }
        public DateTime? CheckListItemsUpdatedAt { get; set; }

        [SQLite.Ignore]
        public Construction Parent { get; set; }
        bool _isStatLoading = true;
        [SQLite.Ignore]
        public bool IsStatLoading { get { return _isStatLoading; } set { _isStatLoading = value; OnPropertyChanged(); } }
        string _problemsCount;
        [SQLite.Ignore]
        public string ProblemsCount { get { return _problemsCount; } set { _problemsCount = value; OnPropertyChanged(); } }
        string _activeFixCount;
        [SQLite.Ignore]
        public string ActiveFixCount { get { return _activeFixCount; } set { _activeFixCount = value; OnPropertyChanged(); } }
        string _completionPercentage;
        [SQLite.Ignore]
        public string CompletionPercentage { get { return _completionPercentage; } set { _completionPercentage = value; OnPropertyChanged(); } }

        public string GetBreadcrumbs() {
            return GetBreadcrumbs(this);
        }

        public string GetBreadcrumbs(Construction item) {
            var breadcrumbs = item.Title;
            if (item.Parent != null)
                breadcrumbs = GetBreadcrumbs(item.Parent) + " > " + breadcrumbs;
            return breadcrumbs;
        }
    }
}
