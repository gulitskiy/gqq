﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.SfChart.XForms;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GQQ.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChartsPage : CarouselPage {
        public SfChart V1Chart { get; set; }
        public SfChart V2Chart { get; set; }
        public SfChart V3Chart { get; set; }

        public ChartsPage ()
		{
			InitializeComponent ();
            V1Chart = TheV1Chart;
            V2Chart = TheV2Chart;
            V3Chart = TheV3Chart;
        }
    }
}