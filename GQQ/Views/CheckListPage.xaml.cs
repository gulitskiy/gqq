﻿using System;
using System.Collections.Generic;
using GQQ.Forms;
using Xamarin.Forms;

namespace GQQ.Views
{
    public partial class CheckListPage : ContentPage
    {
        public ExpandableListView DemandGroupsListView { get; set; }
        public CheckListPage()
        {
			InitializeComponent();
			DemandGroupsListView = TheDemandGroupsListView;
        }
    }
}
