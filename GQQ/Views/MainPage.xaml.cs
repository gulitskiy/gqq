﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace GQQ.Views
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            MasterBehavior = MasterBehavior.Popover;
        }
    }
}
