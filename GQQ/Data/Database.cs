﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GQQ.Common;
using GQQ.Models;
using GQQ.ViewModels;
using PCLStorage;
using SQLite;
using Xamarin.Forms;
using System.Threading;
using GQQ.Services;

namespace GQQ.Data
{
    public class Database : IDisposable
    {
        SQLiteConnection _db;
        static readonly object _locker = new object();

        public Database()
        {
            Monitor.Enter(_locker);
            _db = GetConnection();
            _db.CreateTable<AppDataUpdate>();
            _db.CreateTable<CheckListItem>();
            _db.CreateTable<CheckListItemMessage>();
            _db.CreateTable<Construction>();
            _db.CreateTable<Stat4Construction>();
            _db.CreateTable<Demand>();
            _db.CreateTable<GetStatV1Result>();
            _db.CreateTable<GetStatV2Result>();
            _db.CreateTable<GetStatV3Result>();
        }

        public void Delete<T>(T item) {
            _db.Delete(item);
        }

		public bool IsAppDataObsolete()
		{
			var lastDataUpdate = _db.Table<AppDataUpdate>().OrderByDescending(x => x.UpdatedAt).FirstOrDefault();
			if (lastDataUpdate == null) return true;
			var lastDataUpdateDate = lastDataUpdate.UpdatedAt;
			return lastDataUpdateDate.AddDays(3) < DateTime.Now;
		}

		public void AddOrUpdate<T>(T item) where T : BaseModel, new()
		{
			var oldItem = GetAll<T>(x => x.SyncId == item.SyncId).FirstOrDefault();
			if (item.SyncId != null && oldItem != null)
			{
				item.Id = oldItem.Id;
				_db.Update(item);
			}
			else _db.Insert(item);
		}

		public void AddOrUpdate<T>(List<T> items) where T : BaseModel, new()
		{
			foreach (var item in items)
			{
				AddOrUpdate<T>(item);
			}
		}

		public List<T> GetAll<T>(Expression<Func<T, bool>> predExpr = null)
	        where T : BaseModel, new()
		{
			if (predExpr == null) predExpr = x => true;
			return _db.Table<T>().Where(predExpr).ToList();
		}

		public void UpdateAppData(AppData data)
		{
            AddOrUpdate(data.Constructions.ToList());
			AddOrUpdate(data.Demands.ToList());
			_db.Insert(new AppDataUpdate());
		}

		public int GetDemandsCount(Construction construction)
		{
			var allDemands = GetAll<Demand>();
			var parent = allDemands.First(x => x.Metadata.Contains(construction.Metadata));
			var groups = allDemands.Where(x => x.ParentId == parent.SyncId).Select(x => x.SyncId);

			return _db.Table<Demand>().Count(x => groups.Contains(x.ParentId) && x.Metadata.Contains("rem"));
		}

        public void Clear<T>() where T : BaseModel {
            _db.DeleteAll<T>();
        }

        static SQLiteConnection GetConnection() {
			IFolder rootFolder = FileSystem.Current.LocalStorage;
			IFolder appFolder = rootFolder.CreateFolderAsync("gqq", CreationCollisionOption.OpenIfExists).Result;
            IFile file = appFolder.CreateFileAsync("gqq.db3", CreationCollisionOption.OpenIfExists).Result;
            return new SQLiteConnection(file.Path);
        }

        public void Dispose()
        {
            _db.Dispose();
            Monitor.Exit(_locker);
        }
    }
}
