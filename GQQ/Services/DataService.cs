﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using GQQ.Data;
using GQQ.Models;
using Newtonsoft.Json;
using PCLStorage;
using SQLite;
using System.Threading.Tasks;

namespace GQQ.Services
{
    public class DataService
    {
        static object _locker = new object();

        public HttpClient CreateHttpClient()
        {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(2);
            return client;
        }

        /// <summary>
        /// Возвращает список всех существующих требований (описаний замечаний, remarkDesc)
        /// </summary>
        /// <returns></returns>
        public GetDemandsResult GetDemands()
        {
            GetDemandsResult result = null;

            using (var client = CreateHttpClient())
            {
                var response = client.GetStringAsync("http://green-quarter-quality.com/Api/GetRemarkDescs?all=true").Result;
                result = JsonConvert.DeserializeObject<GetDemandsResult>(response);
            }

            return result;
        }

        /// <summary>
        /// Возвращает список всех существующих объектов (ЖК, блоки, подъезды, этажи, окна...)
        /// </summary>
        /// <returns></returns>
        public GetConstructionsResult GetConstructions()
        {
            GetConstructionsResult result = null;

            using (var client = CreateHttpClient())
            {
                var response = client.GetStringAsync("http://green-quarter-quality.com/Api/GetAreas?all=true").Result;
                result = JsonConvert.DeserializeObject<GetConstructionsResult>(response);
            }

            return result;
        }

        /// <summary>
        /// Возвращает список всех существующих объектов (ЖК, блоки, подъезды, этажи, окна...)
        /// </summary>
        /// <returns></returns>
        public List<CheckListItem> GetCheckListItems(int id)
        {
            List<CheckListItem> result = null;
            using (var db = new Database())
            {
                var construction = db.GetAll<Construction>(x => x.SyncId == id).First();

                if (construction.CheckListItemsUpdatedAt == null || construction.CheckListItemsUpdatedAt.Value.AddDays(1) < DateTime.Now)
                {
                    try
                    {
                        using (var client = CreateHttpClient())
                        {
                            var response = client.GetStringAsync($"http://green-quarter-quality.com/Api/GetRemarksAll?area={id}").Result;
                            result = JsonConvert.DeserializeObject<GetCheckListItemsResult>(response).ToCheckListItems();
                            db.AddOrUpdate(result);
                            foreach (var item in result)
                            {
                                db.AddOrUpdate(item.CheckListItemMessages);
                            }
                            construction.CheckListItemsUpdatedAt = DateTime.Now;
                            db.AddOrUpdate(construction);
                        }
                    }
                    catch (Exception)
                    {
                        result = db.GetAll<CheckListItem>(x => x.ConstructionId == id);
                        foreach (var item in result)
                        {
                            item.CheckListItemMessages = db.GetAll<CheckListItemMessage>(x => x.ParentId == item.SyncId.Value);
                        }
                    }
                }
                else
                {

                    result = db.GetAll<CheckListItem>(x => x.ConstructionId == id);
                    foreach (var item in result)
                    {
                        item.CheckListItemMessages = db.GetAll<CheckListItemMessage>(x => x.ParentId == item.SyncId.Value);
                    }
                }
            }
            return result;
        }

        public Stat4Construction GetStat4Construction(int id, Database db)
        {
            Stat4Construction stat = new Stat4Construction()
            {
                CreatedAt = DateTime.Now.AddYears(-1)
            };

            try
            {
                if (db.GetAll<Stat4Construction>(x => x.ConstructionId == id).Any())
                    stat = db.GetAll<Stat4Construction>(x => x.ConstructionId == id).OrderByDescending(x => x.CreatedAt).First();
            }
            catch (Exception)
            {
            }

            if (stat == null || stat.CreatedAt.AddMinutes(30) < DateTime.Now)
            {
                try
                {
                    using (var client = CreateHttpClient())
                    {
                        client.Timeout = TimeSpan.FromSeconds(10);
                        var response = client.GetStringAsync($"http://green-quarter-quality.com/Api/GetStatForArea?id={id}").Result;
                        var result = JsonConvert.DeserializeObject<GetStat4ConstructionResult>(response);
                        stat = new Stat4Construction()
                        {
                            ConstructionId = id,
                            Value1 = result[0],
                            Value2 = result[1],
                            Value3 = result[2],
                            Value4 = result[3],
                            Value5 = result[4],
                            Value6 = result[5]
                        };
                        db.AddOrUpdate(stat);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("GQQ-DEBUG " + ex.Message);
                }
            }

            return stat;
        }

        //http://green-quarter-quality.com/Api/GetStatForArea?id=24
        public Stat4Construction GetStat4Construction(int id)
        {
            using (var db = new Database())
            {
                return GetStat4Construction(id, db);
            }
        }

        public MemoryStream GetImage(string src)
        {
            var uri = new Uri(src);
            MemoryStream result = new MemoryStream(); ;
            var fileName = Path.GetFileName(src);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder appFolder = rootFolder.CreateFolderAsync("gqq", CreationCollisionOption.OpenIfExists).Result;
            IFolder dest = appFolder.CreateFolderAsync("images", CreationCollisionOption.OpenIfExists).Result;

            if (dest.CheckExistsAsync(fileName).Result == ExistenceCheckResult.FileExists)
            {
                IFile file = dest.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists).Result;
                using (var stream = file.OpenAsync(FileAccess.Read).Result)
                {
                    stream.CopyTo(result);
                    result.Seek(0, SeekOrigin.Begin);
                }
                if (result.Length == 0)
                {
                    file.DeleteAsync().Wait();
                    return GetImage(src);
                }
            }
            else
            {
                WebRequest request = WebRequest.Create(uri);
                IFile file = dest.CreateFileAsync(fileName, CreationCollisionOption.FailIfExists).Result;

                using (Stream stream = request.GetResponseAsync().Result.GetResponseStream())
                {
                    stream.CopyTo(result);
                    result.Seek(0, SeekOrigin.Begin);
                    var buffer = result.ToArray();

                    using (var fileStream = file.OpenAsync(FileAccess.ReadAndWrite).Result)
                    {
                        fileStream.Write(buffer, 0, buffer.Length);
                    }
                    result.Seek(0, SeekOrigin.Begin);
                }
            }

            return result;
        }

        public string SaveCheckListItemPhoto(byte[] data)
        {
            if (data != null && data.Length > 0)
            {
                var fileName = Path.GetFileName(Guid.NewGuid().ToString() + ".jpg");

                IFolder rootFolder = FileSystem.Current.LocalStorage;
                IFolder appFolder = rootFolder.CreateFolderAsync("gqq", CreationCollisionOption.OpenIfExists).Result;
                IFolder dest = appFolder.CreateFolderAsync("images", CreationCollisionOption.OpenIfExists).Result;

                IFile file = dest.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting).Result;

                using (var stream = file.OpenAsync(FileAccess.ReadAndWrite).Result)
                {
                    stream.Write(data, 0, data.Length);
                }

                return file.Path;
            }
            return null;
        }

        public byte[] GetCheckListItemPhoto(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                IFolder rootFolder = FileSystem.Current.LocalStorage;
                IFolder appFolder = rootFolder.CreateFolderAsync("gqq", CreationCollisionOption.OpenIfExists).Result;
                IFolder dest = appFolder.CreateFolderAsync("images", CreationCollisionOption.OpenIfExists).Result;

                if (dest.CheckExistsAsync(Path.GetFileName(path)).Result == ExistenceCheckResult.FileExists)
                {
                    byte[] data;
                    IFile file = dest.CreateFileAsync(path, CreationCollisionOption.OpenIfExists).Result;
                    using (var stream = file.OpenAsync(FileAccess.ReadAndWrite).Result)
                    {
                        data = new byte[stream.Length];
                        stream.Read(data, 0, data.Length);
                    }
                    return data;
                }

            }
            return null;
        }

        public CheckListItemMessage SendMessage(string message, int pid, int areaId, int demandId) {
            var item = new CheckListItemMessage() {
                Text = message,
                ParentId = pid,
                UserName = AuthService.Current.User.UserName
            };

            using (var db = new Database()) {
                db.AddOrUpdate(item);
            }

            string data = "token=" + System.Net.WebUtility.UrlEncode(AuthService.Current.User.Token);
            data += "&remarkId=" + pid;
            data += "&message=" + System.Net.WebUtility.UrlEncode(message);

            using (HttpClient client = CreateHttpClient()) {
                using (StringContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded")) {
                    using (var res = client.PostAsync("http://green-quarter-quality.com/Api/SendMessage", content).Result) {
                        var response = res.Content.ReadAsStringAsync().Result;

                        var saveResult = JsonConvert.DeserializeObject<SaveResult>(response);
                        if (saveResult.messageId > 0) {
                            using (var db = new Database()) {
                                item.SyncId = saveResult.messageId;
                                db.AddOrUpdate(item);
                            }
                        } else {
                            using (var db = new Database()) {
                                db.Delete(item);
                                throw new Exception("Не удалось добавить комментарий.");
                            }
                        }
                    }
                }
            }
            return item;
        }

        public void PostCheckListItem(CheckListItem item)
        {
            using (var db = new Database())
            {
                db.AddOrUpdate(item);
            }
            var photoData = GetCheckListItemPhoto(item.Photo);

            if (string.IsNullOrEmpty(item.Message))
                item.Message = "Нет нареканий";

            string data = "token=" + System.Net.WebUtility.UrlEncode(AuthService.Current.User.Token);
            data += "&areaId=" + item.ConstructionId;
            data += "&descId=" + item.DemandId;
            data += "&status=" + item.Status;
            if (photoData != null && photoData.Length > 0)
                data += "&photo=" + System.Net.WebUtility.UrlEncode(Convert.ToBase64String(photoData));
            if(!string.IsNullOrEmpty(item.Message))
                data += "&message=" + System.Net.WebUtility.UrlEncode(item.Message);

            using (HttpClient client = CreateHttpClient())
            {
                using (StringContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded"))
                {
                    using (var res = client.PostAsync("http://green-quarter-quality.com/Api/UploadPhoto", content).Result)
                    {
                        var response = res.Content.ReadAsStringAsync().Result;

                        var saveResult = JsonConvert.DeserializeObject<SaveResult>(response);
                        if (saveResult.remarkId > 0)
                        {
                            using (var db = new Database())
                            {

                                item.SyncId = saveResult.remarkId;
                                db.AddOrUpdate(item);
                            }
                        }
                        else
                        {
                            using (var db = new Database())
                            {
                                db.Delete(item);
                                throw new Exception("Не удалось сохранить замечание.");
                            }

                        }
                    }
                }
            }
        }

        public void PostNotSyncCheckListItem(CheckListItem item)
        {
            HttpClient client = new HttpClient();

            var photoData = GetCheckListItemPhoto(item.Photo);

            string data = "token=" + System.Net.WebUtility.UrlEncode(AuthService.Current.User.Token);
            data += "&areaId=" + item.ConstructionId;
            data += "&descId=" + item.DemandId;
            data += "&status=" + item.Status;
            data += "&photo=" + System.Net.WebUtility.UrlEncode(Convert.ToBase64String(photoData));
            data += "&message=" + System.Net.WebUtility.UrlEncode(item.Message);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            var res = client.PostAsync("http://green-quarter-quality.com/Api/UploadPhoto", content).Result;
            var response = res.Content.ReadAsStringAsync().Result;

            using (var db = new Database())
            {
                var saveResult = JsonConvert.DeserializeObject<SaveResult>(response);
                item.SyncId = saveResult.remarkId;
                db.AddOrUpdate(item);
            }
        }

        public void DoSync()
        {
            var db = new Database();
            var items = db.GetAll<CheckListItem>(x => x.SyncId == null || x.SyncId == 0);
            db.Dispose();
            if (items.Count > 0)
                foreach (var item in items)
                {
                    PostNotSyncCheckListItem(item);
                }
        }

        public GetFullStatResult GetFullStat()
        {
            IEnumerable<GetStatV1Result> v1Results = null;
            IEnumerable<GetStatV2Result> v2Results = null;
            IEnumerable<GetStatV3Result> v3Results = null;
            using (var db = new Database())
            {
                Task.WaitAll(new Task[] {
                    Task.Run(() => v1Results = GetStatV1(db)),
                    Task.Run(() => v2Results = GetStatV2(db)),
                    Task.Run(() => v3Results = GetStatV3(db))
                });
            }
            return new GetFullStatResult()
            {
                V1 = v1Results,
                V2 = v2Results,
                V3 = v3Results
            };
        }

        public IEnumerable<GetStatV1Result> GetStatV1(Database db)
        {
            IEnumerable<GetStatV1Result> result = null;
            lock (_locker)
            {
                var item = db.GetAll<GetStatV1Result>().FirstOrDefault();
                if (item != null && item.CreatedAt.AddDays(3) > DateTime.Now)
                    result = db.GetAll<GetStatV1Result>();
            }
            if (result == null)
                try
                {
                    HttpClient client = new HttpClient();
                    var response = client.GetStringAsync("http://green-quarter-quality.com/api/GetStatsV1").Result;
                    result = JsonConvert.DeserializeObject<GetStatV1Results>(response);
                    lock (_locker)
                    {
                        db.Clear<GetStatV1Result>();
                        db.AddOrUpdate(result.ToList());
                    }
                }
                catch (Exception)
                {
                    lock (_locker)
                        result = (GetStatV1Results)db.GetAll<GetStatV1Result>();
                }
            if (result == null) result = new GetStatV1Results();
            return result;
        }

        public IEnumerable<GetStatV2Result> GetStatV2(Database db)
        {
            IEnumerable<GetStatV2Result> result = null;
            lock (_locker)
            {
                var item = db.GetAll<GetStatV2Result>().FirstOrDefault();
                if (item != null && item.CreatedAt.AddDays(3) > DateTime.Now)
                    result = db.GetAll<GetStatV2Result>();
            }
            if (result == null)
                try
                {
                    HttpClient client = new HttpClient();
                    var response = client.GetStringAsync("http://green-quarter-quality.com/api/GetStatsV2").Result;
                    result = JsonConvert.DeserializeObject<GetStatV2Results>(response);
                    lock (_locker)
                    {
                        db.Clear<GetStatV2Result>();
                        db.AddOrUpdate(result.ToList());
                    }
                }
                catch (Exception)
                {
                    lock (_locker)
                        result = (GetStatV2Results)db.GetAll<GetStatV2Result>();
                }
            if (result == null) result = new GetStatV2Results();
            return result.GroupBy(x => x.name).Select(x => new GetStatV2Result()
            {
                name = x.Key,
                value = x.Sum(r => r.value)
            });
        }

        public IEnumerable<GetStatV3Result> GetStatV3(Database db)
        {
            IEnumerable<GetStatV3Result> result = null;
            lock (_locker)
            {
                var item = db.GetAll<GetStatV3Result>().FirstOrDefault();
                if (item != null && item.CreatedAt.AddDays(3) > DateTime.Now)
                    result = db.GetAll<GetStatV3Result>();
            }
            if (result == null)
                try
                {
                    HttpClient client = new HttpClient();
                    var response = client.GetStringAsync("http://green-quarter-quality.com/api/GetStatsV3").Result;
                    result = JsonConvert.DeserializeObject<GetStatV3Results>(response);
                    lock (_locker)
                    {
                        db.Clear<GetStatV3Result>();
                        db.AddOrUpdate(result.ToList());
                    }
                }
                catch (Exception)
                {
                    lock (_locker)
                        result = (GetStatV3Results)db.GetAll<GetStatV3Result>();
                }
            if (result == null) result = new GetStatV3Results();
            return result.GroupBy(x => x.text).Select(x => new GetStatV3Result()
            {
                text = x.Key.Length > 35 ? x.Key.Substring(0, 35) + "..." : x.Key,
                value = x.Sum(r => r.value)
            });
        }
    }

    public class GetFullStatResult
    {
        public IEnumerable<GetStatV1Result> V1 { get; set; }
        public IEnumerable<GetStatV2Result> V2 { get; set; }
        public IEnumerable<GetStatV3Result> V3 { get; set; }

        public GetFullStatResult()
        {
            V1 = new GetStatV1Results();
            V2 = new GetStatV2Results();
            V3 = new GetStatV3Results();
        }

        public class GetStatV1ResultEx
        {
            public string Name { get; set; }
            public int Value { get; set; }
        }
    }

    public class GetStatV1Results : List<GetStatV1Result>
    {
    }

    public class GetStatV2Results : List<GetStatV2Result>
    {
    }

    public class GetStatV3Results : List<GetStatV3Result>
    {
    }

    public class GetStatV1Result : BaseModel
    {
        public string name { get; set; }
        public int count { get; set; }
        public int totalEr { get; set; }
        public int activeEr { get; set; }
        public DateTime CreatedAt { get; set; }

        public GetStatV1Result()
        {
            CreatedAt = DateTime.Now;
        }
    }

    public class GetStatV2Result : BaseModel
    {
        public string name { get; set; }
        public int value { get; set; }
        public DateTime CreatedAt { get; set; }

        public GetStatV2Result()
        {
            CreatedAt = DateTime.Now;
        }
    }

    public class GetStatV3Result : BaseModel
    {
        public string text { get; set; }
        public int value { get; set; }
        public DateTime CreatedAt { get; set; }

        public GetStatV3Result()
        {
            CreatedAt = DateTime.Now;
        }
    }

    class SaveResult
    {
        public int remarkId { get; set; }
        public int messageId { get; set; }
    }

    public class GetCheckListItemsResult : List<GetCheckListItemResult>
    {
        public List<CheckListItem> ToCheckListItems()
        {
            return this.Select(x => x.ToCheckListItem()).ToList();
        }
    }

    public class GetCheckListItemResult
    {
        public int id { get; set; }
        public string imagePath { get; set; }
        public DateTime createDate { get; set; }
        public string metadata { get; set; }
        public int areaId { get; set; }
        public int descriptionId { get; set; }
        public string normalizedUserName { get; set; }
        public List<object> remarkExtension { get; set; }
        public List<GetCheckListItemMessageResult> messages { get; set; }

        public GetCheckListItemResult()
        {
            messages = new List<GetCheckListItemMessageResult>();
            remarkExtension = new List<object>();
        }

        public CheckListItem ToCheckListItem()
        {
            var item = new CheckListItem()
            {
                CreatedAt = createDate,
                ConstructionId = areaId,
                DemandId = descriptionId,
                Photo = imagePath,
                Status = metadata.Contains("new") ? 1 : 0,
                SyncId = id
            };
            item.CheckListItemMessages = messages.Select(x =>
            {
                var message = x.ToCheckListItemMessage();
                message.ParentId = id;
                return message;
            }).ToList();
            return item;
        }

        public class GetCheckListItemMessageResult
        {
            public int id { get; set; }
            public string text { get; set; }
            public string normalizedUserName { get; set; }
            public DateTime createDate { get; set; }

            public CheckListItemMessage ToCheckListItemMessage()
            {
                CheckListItemMessage item = new CheckListItemMessage()
                {
                    CreatedAt = createDate,
                    SyncId = id,
                    Text = text,
                    UserName = normalizedUserName
                };
                return item;
            }
        }
    }

    public class GetDemandsResult : List<GetDemandsItemResult>
    {
        public List<Demand> ToDemands()
        {
            return this.Select(x => new Demand()
            {
                Description = x.longDescriptionRu,
                SyncId = x.id,
                Metadata = x.metadata,
                ParentId = x.parentId,
                Title = x.descriptionRu,
                Weight = x.weight
            }).ToList();
        }
    }

    public class GetConstructionsResult : List<GetConstructionsItemResult>
    {
        public List<Construction> ToConstructions()
        {
            return this.Select(x => new Construction()
            {
                CenterX = x.dx,
                CenterY = x.dy,
                SyncId = x.id,
                Metadata = x.metadata,
                ParentId = x.parentId,
                Picture = x.picture,
                Title = x.nameRu
            }).ToList();
        }
    }

    public class GetStat4ConstructionResult : List<double>
    {

    }

    public class GetDemandsItemResult
    {
        public int id { get; set; }
        public string descriptionRu { get; set; }
        public string metadata { get; set; }
        public int weight { get; set; }
        public int? parentId { get; set; }
        public string longDescriptionRu { get; set; }
    }

    public class GetConstructionsItemResult
    {
        public int id { get; set; }
        public string nameRu { get; set; }
        public string picture { get; set; }
        public float? dx { get; set; }
        public float? dy { get; set; }
        public string metadata { get; set; }
        public int? parentId { get; set; }
    }
}
