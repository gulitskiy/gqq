﻿using System;
using System.Diagnostics;
using System.Net.Http;
using GQQ.Models;
using Newtonsoft.Json;
using PCLStorage;
using System.Collections.Generic;
using System.Linq;

namespace GQQ.Services
{
	public class AuthService
	{
        string _authFileName = ".auth";
		static AuthService _current;
		public static AuthService Current
		{
			get
			{
				if (_current == null) _current = new AuthService();
				return _current;
			}
		}

		User _user = null;
		public User User { get { return _user; } set { _user = value; } }

		bool _isAuthenticated = false;
		public bool IsAuthenticated
		{
			get
			{
				return _isAuthenticated;
			}
			set
			{
				_isAuthenticated = value;
				if (value)
				{
					AfterSignIn?.Invoke();
				}
				else
				{
					AfterSignOut?.Invoke();
				}
			}
		}

		public AuthService()
		{
			Initialize();
		}

		IFolder GetAppFolder()
		{
			IFolder rootFolder = FileSystem.Current.LocalStorage;
			return rootFolder.CreateFolderAsync("gqq", CreationCollisionOption.OpenIfExists).Result;
		}

		void Initialize()
		{
			IFolder appFolder = GetAppFolder();

			var existenceCheckResult = appFolder.CheckExistsAsync(_authFileName).Result;
			if (existenceCheckResult != ExistenceCheckResult.FileExists) return;

			IFile file = appFolder.GetFileAsync(_authFileName).Result;
			try
			{
				var userJson = file.ReadAllTextAsync().Result;
				User = JsonConvert.DeserializeObject<User>(userJson);
				IsAuthenticated = true;
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}

		void SignIn(User user)
		{
			IFolder appFolder = GetAppFolder();
			IFile file = appFolder.CreateFileAsync(_authFileName, CreationCollisionOption.ReplaceExisting).Result;
			try
			{
				var userJson = JsonConvert.SerializeObject(user);
				file.WriteAllTextAsync(userJson).Wait();
				User = user;
				IsAuthenticated = true;
			}
			catch (Exception)
			{

			}
		}

		public SignInResult SignIn(string userName, string password)
		{
			SignInResult result = null;
			try
			{
				HttpClient client = new HttpClient();
				var uri = new Uri(string.Format("http://green-quarter-quality.com/Api/Login?login={0}&password={1}", userName, password));
				var response = client.GetAsync(uri).Result;
				if (response.IsSuccessStatusCode)
				{
					var content = response.Content.ReadAsStringAsync().Result;
					result = JsonConvert.DeserializeObject<SignInResult>(content);
                    var accessData = GetAccessData();
                    var userId = result.Token.Split("|||".ToCharArray())[0];
                    var userAreas = accessData.Where(x => x.UserId.Equals(userId)).Select(x => x.AreaId);
                    var userRoleName = accessData.First(x => x.UserId.Equals(userId)).Metadata;
                    if (string.IsNullOrEmpty(result.Error)) {
                        SignIn(new User() {
                            Token = result.Token,
                            UserName = result.UserName,
                            RoleName = userRoleName,
                            Areas = new List<int>(userAreas)
                        });
                    }
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return result;
		}

        public GetAccessDataResults GetAccessData() {
            HttpClient client = new HttpClient();
            var uri = new Uri("http://green-quarter-quality.com/api/getaccessdata");
            var response = client.GetAsync(uri).Result;
            var accessData = JsonConvert.DeserializeObject<GetAccessDataResults>(response.Content.ReadAsStringAsync().Result);
            return accessData;
        }

		public void SignOut()
		{
			IFolder appFolder = GetAppFolder();
			IFile file = appFolder.CreateFileAsync(_authFileName, CreationCollisionOption.ReplaceExisting).Result;
			file.DeleteAsync().Wait();
			User = null;
			IsAuthenticated = false;
		}

		public event Action AfterSignIn;
		public event Action AfterSignOut;
	}

	public class SignInResult
	{
		public string Token { get; set; }
		public string UserName { get; set; }
		public string Error { get; set; }
	}

    public class GetAccessDataResult {
        public int AreaId { get; set; }
        public string UserId { get; set; }
        public string Metadata { get; set; }
    }

    public class GetAccessDataResults : List<GetAccessDataResult> {

    }
}
